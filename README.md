## What is this? 

This image is based on [`raynera/base`](https://registry.hub.docker.com/u/raynera/base/) with Oracle JDK 8 installed.

![logo](http://upload.wikimedia.org/wikipedia/commons/thumb/4/40/Wave.svg/35px-Wave.svg.png)