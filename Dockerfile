FROM raynera/base
MAINTAINER Andrew Rayner <a.d.rayner@gmail.com>

ENV VERSION 8
ENV UPDATE 45
ENV BUILD 14

ENV JAVA_HOME /usr/lib/jvm/java-${VERSION}-oracle

RUN mkdir -p ${JAVA_HOME} \
	&& curl --silent --location --retry 3 \
		--cacert /etc/ssl/certs/GeoTrust_Global_CA.pem \
		--header "Cookie: oraclelicense=accept-securebackup-cookie;" \
		http://download.oracle.com/otn-pub/java/jdk/"${VERSION}"u"${UPDATE}"-b"${BUILD}"/jdk-"${VERSION}"u"${UPDATE}"-linux-x64.tar.gz | \
		tar -xzv --strip-components=1 -C ${JAVA_HOME} \
    && rm -Rf ${JAVA_HOME}/lib/missioncontrol ${JAVA_HOME}/lib/visualvm 
    
RUN update-alternatives --install "/usr/bin/java" "java" "${JAVA_HOME}/bin/java" 1 \
	&& update-alternatives --install "/usr/bin/javaws" "javaws" "${JAVA_HOME}/bin/javaws" 1 \
	&& update-alternatives --install "/usr/bin/javac" "javac" "${JAVA_HOME}/bin/javac" 1 \
	&& update-alternatives --set java "${JAVA_HOME}/bin/java" \
	&& update-alternatives --set javaws "${JAVA_HOME}/bin/javaws" \
	&& update-alternatives --set javac "${JAVA_HOME}/bin/javac"
